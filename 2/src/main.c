#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>
#include "io.h"
#include "array_worker.h"
#include "utils.h"

#define ERR_NO_ELEMENTS 1
#define ERR_COUNT_PARAMS 3
#define ERR_OPEN_IN_FILE 4
#define ERR_OPEN_OUT_FILE 8

int main(int argc, char **argv)
{
    FILE *input;
    size_t n = 0;
    int rc = 0;
    int *array;

    if (argc < 3)
        return ERR_COUNT_PARAMS;

    if ((input = fopen(argv[1], "r")) == NULL)
        return ERR_OPEN_IN_FILE;

    if ((rc = find_array_size(input, &n)) != 0)
    {
        fclose(input);
        return rc;
    }

    if ((rc = get_array_from_file(input, &array, n)) != 0)
    {
        fclose(input);
        return rc;
    }

    fclose(input);

    int *pb = NULL;
    int *pe = NULL;

    void *hlib_filter = dlopen("libfilter.so", RTLD_NOW);
    if (!hlib_filter)
    int (*key)(const int *, const int *, int **, int **);
    key = (int (*)(const int *, const int *, int **, int **))dlsym(hlib_filter, "key");
    void (*mysort)(void *, size_t, size_t, int (*compar)(const void *, const void *));
    mysort = (void (*)(void *, size_t, size_t, int (*compar)(const void *, const void *)))dlsym(hlib_filter, "mysort");

    if (argv[3] != NULL)
    {
        if (strcmp(argv[3], "f") == 0)
        {
            if ((rc = key(array, array + n, &pb, &pe)) != 0)
            {
                free(array);
                return rc;
            }
        }
        else
        {
            free(array);
            return -1;
        }
    }

    FILE *output;

    output = fopen(argv[2], "wt");

    if (output == NULL)
    {
        free(array);
        if (!pb)
            free(pb);
        return ERR_OPEN_OUT_FILE;
    }

    if (pb != NULL)
    {
        mysort(pb, pe - pb, sizeof(int), compare_int);
        print_array(output, pb, pe);
        free(pb);
    }
    else
    {
        mysort(array, n, sizeof(int), compare_int);
        print_array(output, array, array + n);
    }

    free(array);
    fclose(output);

    return rc;
}
