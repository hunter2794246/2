#include <stdio.h>
#include "io.h"
#include "malloc.h"
#include "stdlib.h"

#define ERR_READING_FILE 5
#define ERR_NO_ELEMENTS 6
#define ERR_ALLOC_SRC_ARR 7

void print_array(FILE *out, int *arr_start, int *arr_end)
{
    for (int *i = arr_start; i < arr_end; i++)
        fprintf(out, "%d ", *i);
    printf("\n");
}

int find_array_size(FILE *input, size_t *n)
{
    int k;

    while (fscanf(input, "%d", &k) == 1)
    {
        (*n)++;
    }

    if (*n == 0)
    {
        return ERR_NO_ELEMENTS;
    }
    return EXIT_SUCCESS;
}

int get_array_from_file(FILE *input, int **array, size_t size)
{
    int *p;

    p = malloc(size * sizeof(int));

    if (p == NULL)
    {
        return ERR_ALLOC_SRC_ARR;
    }

    if (fseek(input, SEEK_SET, 0) != 0)
    {
        free(p);
        return ERR_READING_FILE;
    }

    for (size_t i = 0; i < size; i++)
    {
        if (fscanf(input, "%d", p + i) != 1)
        {
            free(p);
            return ERR_READING_FILE;
        }
    }

    *array = p;

    return EXIT_SUCCESS;
}
