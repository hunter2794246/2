#include "utils.h"


int compare_int(const void *a, const void *b)
{
    int *a1 = (int *) a;
    int *b1 = (int *) b;

    if (*a1 > *b1)
        return 1;
    if (*a1 < *b1)
        return -1;
    return 0;
}

void swap(void *p1, void *p2, const size_t size)
{
    char buff;
    for (int i = 0; i < size; i++)
    {
        buff = *((char *) p1 + i);
        *((char *) p1 + i) = *((char *) p2 + i);
        *((char *) p2 + i) = buff;
    }
}
