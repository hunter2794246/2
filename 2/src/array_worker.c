#include "array_worker.h"
#include "utils.h"
#define ERR_ALLOC_DST_ARR 2
#define ERR_INCORRECT_PARAMS 9
#define ERR_NO_ELEMENTS 10

void mysort(void *base, size_t nmemb, size_t size, int (*compar)(const void *, const void *))
{
    for (; nmemb > 0; nmemb--)
    {
        for (size_t i = 1; i < nmemb; i++)
        {
            if (compar((char *) base + size * (i - 1), (char *) base + size * i) > 0)
            {
                swap((char *) base + size * (i - 1), (char *) base + size * i, size);
            }
        }
    }
}


int key(const int *pb_src, const int *pe_src, int **pb_dst, int **pe_dst)
{
    if (!pb_src || !pe_src || !pb_dst || !pe_dst)
        return ERR_INCORRECT_PARAMS;

    if (pb_src > pe_src)
        return ERR_INCORRECT_PARAMS;

    int min = *pb_src;
    const int *i_min = pb_src;
    int max = *pb_src;
    const int *i_max = pb_src;

    for (const int *i = pb_src; i < pe_src; i++)
    {
        if (*i < min)
        {
            min = *i;
            i_min = i;
        }
    }
    for (const int *i = pb_src; i < pe_src; i++)
    {
        if (*i > max)
        {
            max = *i;
            i_max = i;
        }
    }

    const int *b_dst = (i_min < i_max ? i_min : i_max) + 1;
    const int *e_dst = (i_min > i_max ? i_min : i_max);

    if (e_dst - b_dst < 1)
        return ERR_NO_ELEMENTS;

    int *p = malloc((size_t) e_dst - (size_t) b_dst);

    if (p == NULL)
        return ERR_ALLOC_DST_ARR;


    *pb_dst = p;
    *pe_dst = p + (e_dst - b_dst);
    int j = 0;
    for (const int *i = b_dst; i < e_dst; i++)
    {
        *((*pb_dst) + j) = *i;
        j++;
    }

    return EXIT_SUCCESS;
}