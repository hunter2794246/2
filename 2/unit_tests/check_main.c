#include <stdlib.h>
#include <check.h>
#include "check_array_worker.h"

int main(void)
{
    int test_suites_cnt = 2, failed_tests_cnt = 0;
    Suite *s[] = { mysort_suite(), key_suite() };

    for (int i = 0; i < test_suites_cnt; i++)
    {
        SRunner *runner = srunner_create(s[i]);
        srunner_run_all(runner, CK_VERBOSE);
        failed_tests_cnt += srunner_ntests_failed(runner);
        srunner_free(runner);
    }
    
    return (failed_tests_cnt == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
