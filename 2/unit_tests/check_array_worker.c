#include <check.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include "array_worker.h"
#include "utils.h"


START_TEST(test_sort_ordered_array)
{
    int elements_count = 5;
    int res[5] = {1, 2, 3, 4, 5};
    int myres[5] = {1, 2, 3, 4, 5};

    void *hlib_filter = dlopen("libfilter.so", RTLD_NOW);
    if (!hlib_filter)
    void (*mysort)(void *, size_t, size_t, int (*compar)(const void *, const void *));
    mysort = (void (*)(void *, size_t, size_t, int (*compar)(const void *, const void *)))dlsym(hlib_filter, "mysort");

    qsort(res, elements_count, sizeof(int), compare_int);
    mysort(myres, elements_count, sizeof(int), compare_int);

    dlclose(hlib_filter);

    ck_assert_mem_eq(res, myres, elements_count * sizeof(int));
}
END_TEST

START_TEST(test_sort_oneelement_array)
    {
        int elements_count = 1;
        int res[1] = {1};
        int myres[1] = {1};

        void *hlib_filter = dlopen("libfilter.so", RTLD_NOW);
        if (!hlib_filter)void (*mysort)(void *, size_t, size_t, int (*compar)(const void *, const void *));
        mysort = (void (*)(void *, size_t, size_t, int (*compar)(const void *, const void *)))dlsym(hlib_filter, "mysort");


        qsort(res, elements_count, sizeof(int), compare_int);
        mysort(myres, elements_count, sizeof(int), compare_int);

        dlclose(hlib_filter);

        ck_assert_mem_eq(res, myres, elements_count * sizeof(int));
    }
END_TEST

START_TEST(test_sort_equal_array)
    {
        int elements_count = 5;
        int res[5] = {1, 1, 1, 1, 1};
        int myres[5] = {1, 1, 1, 1, 1};

        void *hlib_filter = dlopen("libfilter.so", RTLD_NOW);
        if (!hlib_filter)void (*mysort)(void *, size_t, size_t, int (*compar)(const void *, const void *));
        mysort = (void (*)(void *, size_t, size_t, int (*compar)(const void *, const void *)))dlsym(hlib_filter, "mysort");

        qsort(res, elements_count, sizeof(int), compare_int);
        mysort(myres, elements_count, sizeof(int), compare_int);
        dlclose(hlib_filter);
        ck_assert_mem_eq(res, myres, elements_count * sizeof(int));
    }
END_TEST

START_TEST(test_sort_unsorted_array)
    {
        int elements_count = 5;
        int res[5] = {5, 4, 3, 2, 1};
        int myres[5] = {5, 4, 3, 2, 1};

        void *hlib_filter = dlopen("libfilter.so", RTLD_NOW);
        if (!hlib_filter)void (*mysort)(void *, size_t, size_t, int (*compar)(const void *, const void *));
        mysort = (void (*)(void *, size_t, size_t, int (*compar)(const void *, const void *)))dlsym(hlib_filter, "mysort");

        qsort(res, elements_count, sizeof(int), compare_int);
        mysort(myres, elements_count, sizeof(int), compare_int);
        dlclose(hlib_filter);
        ck_assert_mem_eq(res, myres, elements_count * sizeof(int));
    }
END_TEST

START_TEST(test_sort_random_array)
    {
        int elements_count = 5;
        int res[5] = {1, 7, 5, 2, 1};
        int myres[5] = {1, 7, 5, 2, 1};

        void *hlib_filter = dlopen("libfilter.so", RTLD_NOW);
        if (!hlib_filter)void (*mysort)(void *, size_t, size_t, int (*compar)(const void *, const void *));
        mysort = (void (*)(void *, size_t, size_t, int (*compar)(const void *, const void *)))dlsym(hlib_filter, "mysort");

        qsort(res, elements_count, sizeof(int), compare_int);
        mysort(myres, elements_count, sizeof(int), compare_int);
        dlclose(hlib_filter);
        ck_assert_mem_eq(res, myres, elements_count * sizeof(int));
    }
END_TEST

Suite *mysort_suite(void)
{
    Suite *s = suite_create("test_sort");

    TCase *tc_c = tcase_create("Sort");

    tcase_add_test(tc_c, test_sort_ordered_array);
    tcase_add_test(tc_c, test_sort_oneelement_array);
    tcase_add_test(tc_c, test_sort_equal_array);
    tcase_add_test(tc_c, test_sort_unsorted_array);
    tcase_add_test(tc_c, test_sort_random_array);
    suite_add_tcase(s, tc_c);

    return s;
}

START_TEST(test_key_invalid_null_parametrs)
    {
        ck_assert_int_ne(key(NULL, NULL, NULL, NULL), EXIT_SUCCESS);
    }
END_TEST

START_TEST(test_key_invalid_parametrs)
    {
        int elements_count = 5;
        int myres[5] = {1, 7, 5, 2, 1};

        int *pb_src = myres + elements_count;
        int *pe_src = myres;
        int *pb_dst;
        int *pe_dst;

        ck_assert_int_ne(key(pb_src, pe_src, &pb_dst, &pe_dst), EXIT_SUCCESS);
    }
END_TEST

START_TEST(test_key_no_elements)
    {
        int elements_count = 5;
        int myres[5] = {7, 1, 5, 2, 1};

        int *pb_src = myres;
        int *pe_src = myres + elements_count;
        int *pb_dst;
        int *pe_dst;

        ck_assert_int_ne(key(pb_src, pe_src, &pb_dst, &pe_dst), EXIT_SUCCESS);
    }
END_TEST


START_TEST(test_key_one_element)
    {
        int elements_count = 5;
        int myres[5] = {5, 1, 5, 7, 1};
        int res[] = {5};
        int *pb_src = myres;
        int *pe_src = myres + elements_count;
        int *pb_dst;
        int *pe_dst;

        ck_assert_int_eq(key(pb_src, pe_src, &pb_dst, &pe_dst), EXIT_SUCCESS);
        ck_assert_mem_eq(res, pb_dst, sizeof (res));
        free(pb_dst);
    }
END_TEST

START_TEST(test_key_usual_element)
    {
        int elements_count = 6;
        int myres[6] = {0, 1, 2, 3, 4, 5};
        int res[] = {1, 2, 3, 4};
        int *pb_src = myres;
        int *pe_src = myres + elements_count;
        int *pb_dst;
        int *pe_dst;

        ck_assert_int_eq(key(pb_src, pe_src, &pb_dst, &pe_dst), EXIT_SUCCESS);
        ck_assert_mem_eq(res, pb_dst, sizeof (res));
        free(pb_dst);
    }
END_TEST

Suite *key_suite(void)
{
    Suite *s = suite_create("test_key");

    TCase *tc_c = tcase_create("Key");

    tcase_add_test(tc_c, test_key_invalid_parametrs);
    tcase_add_test(tc_c, test_key_invalid_null_parametrs);
    tcase_add_test(tc_c, test_key_no_elements);
    tcase_add_test(tc_c, test_key_one_element);
    tcase_add_test(tc_c, test_key_usual_element);
    suite_add_tcase(s, tc_c);

    return s;
}
