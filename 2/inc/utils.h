#ifndef LAB_07_01_01_UTILS_H
#define LAB_07_01_01_UTILS_H

#include "stdlib.h"

int compare_int(const void *a, const void *b);
void swap(void *p1, void *p2, const size_t size);

#endif //LAB_07_01_01_UTILS_H
