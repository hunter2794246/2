#ifndef LAB_07_01_01_ARRAY_WORKER_H
#define LAB_07_01_01_ARRAY_WORKER_H

#include "stdlib.h"

void mysort(void *base, size_t nmemb, size_t size, int (*compar)(const void *, const void *));
int key(const int *pb_src, const int *pe_src, int **pb_dst, int **pe_dst);

#endif //LAB_07_01_01_ARRAY_WORKER_H
