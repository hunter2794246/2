#ifndef LAB_07_01_01_IO_H
#define LAB_07_01_01_IO_H

void print_array(FILE *out, int *arr_start, int *arr_end);
int find_array_size(FILE *input, size_t *n);
int get_array_from_file(FILE *input, int **array, size_t size);

#endif //LAB_07_01_01_IO_H
